(in-package :cl-user)
(defpackage tootsville.web
  (:documentation  "This package  contains  the  handlers that  directly
 handle  various URI  routes for  the web  server. These  are generally
 invoked currently via FastCGI, or a running Hunchentoot instance.")
  (:use :alexandria :cl
        :caveman2 :tootsville.config
        :tootsville.view :tootsville.db
        :datafly :sxql)
  (:import-from :split-sequence :split-sequence)
  (:export #:*web*)
  (:export #:jscl-rebuild-when-needed
           #:jscl-rebuild-thread
           #:jscl-rebuild-if-needed
           #:wants-json-p))
(in-package :tootsville.web)

;; for @route annotation
(syntax:use-syntax :annot)

;; Application

(defclass <web> (<app>) ()
  (:documentation  "This is  the  local specialization  of the  Caveman2
 application class."))
(defparameter *web* (make-instance '<web>)
  "This is the singleton instance of the web application object.")
(clear-routing-rules *web*)



(setf (ningle:requirement *web* :content-type)
      (lambda (content-type)
        (etypecase content-type
          (string (string-equal content-type (request-content-type *request*)))
          (cons (member (request-content-type *request*) content-type
                        :test #'string-equal)))))



(defun wants-json-p ()
  "Does the client request Accept JSON format?"
  (let ((accept (gethash "Accept" (request-headers *request*))))
    (or (search "application/json" accept)
        (search "text/json" accept)
        (search "application/x-json" accept)
        (search ".js" (request-uri *request*)))))

(defun redirect-to/html-body (uri)
  "Returns an octet array that gives a simple redirection link.

This is  a silly  legacy thing  for ancient  browsers that  don't follow
a  3xx   redirection  or  want   to  display  something   while  they're
redirecting. In  real life, it's  rarely encountered by a  real browser,
but sometimes caught by tools like curl or wget with certain settings."
  (flexi-streams:string-to-octets
   (concatenate 'string
                "<!DOCTYPE html><html><title>Redirect</title><a href="
                (string #\quotation_mark)
                uri
                (string #\quotation_mark)
                ">Redirected to "
                uri
                "</a></html>")))


;;; Default route

(defroute route-/ "/" () (redirect-to "https://www.tootsville.org/"))



(define-condition unimplemented (error)
  ()
  (:documentation "Signals that a feature has not been inmplemented yet"))

(define-constant +application/json+ "application/json"
  :test 'equal
  :documentation "The string application/json, since we use it so often.")


;; Error pages

(defmethod on-exception ((app <web>) code)
  "Return error with code CODE

CODE is allowed to be a string beginning with an HTTP error code.

CODE must be between 300-599, inclusive, or 501 will be used.

TODO: We SHOULD validate that CODE is a sane HTTP error code, but we don't."
  (declare (ignore app)) 
  (cond
    ((consp code) 
     (render-json code))
    ((wants-json-p)
     (render-json `((:error . ,code))))
    (t (let ((code-number (typecase code
                            (number code)
                            (string (parse-integer code :junk-allowed t)) 
                            (caveman2.exception:http-exception 
                             (caveman2.exception:exception-code code))
                            (t 501)))) 
         (unless (<= 300 code-number 599)
           (setf code-number 501))
         (redirect-to (format nil "https://www.tootsville.org/error/~d.shtml" code-number))))))



(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun after-slash (s)
    (if (find #\/ s)
        (subseq (string-downcase s) (1+ (position #\/ s)))
        (string-downcase s))))

(defmacro check-arg-type (arg type &optional name)
  `(unless (typep ,arg ',type)
     (return-from endpoint
       (list 400
             (list :content-type "application/json")
             (render-json
              (list :error 
                    ,(format nil "~:(~a~) provided is not a valid ~a"
                             arg (or name (string-capitalize type)))
                    :expected-type ,(or name (string-capitalize type))
                    :argument-name ,(string-capitalize arg)
                    :read (format nil "~s" ,arg)))))))

(defmacro defendpoint ((method uri (&rest arglist) &optional accept-type)
                       &body body)
  (let ((fname (intern (string-upcase 
                        (format nil "~a-~a~@[~a~]"
                                method
                                (remove-if (lambda (ch)
                                             (or (char= #\: ch)
                                                 (not (sb-impl::constituentp
                                                       ch *readtable*))))
                                           uri)
                                (cond
                                  ((null accept-type) nil)
                                  ((consp accept-type) 
                                   (format nil "~{.~a~}"
                                           (mapcar #'after-slash accept-type)))
                                  ((stringp accept-type)
                                   (after-slash accept-type))))))))
    `(defroute ,fname (,uri :method ,method
                            ,@(when accept-type
                                (list :accept
                                      (if (consp accept-type)
                                          (list 'quote accept-type)
                                          accept-type))))
       (&key ,@arglist)
       (block endpoint 
         (block ,fname
           ,@body)))))
