(in-package :tootsville.web)
(syntax:use-syntax :annot)

(defvar *player* nil)



(defun potential-toot-name-character-p (c)
  (and (characterp c)
       (or (alphanumericp c)
           (char= #\- c)
           (char= #\' c)
           (char= #\space c))))

(defun potential-toot-name-p (toot-name)
  (and (stringp toot-name)
       (<= 3 (length toot-name) 64)
       (every #'potential-toot-name-character-p
              toot-name)
       (alpha-char-p (char toot-name 0))))

(deftype toot-name ()
  `(and string (satisfies potential-toot-name-p)))

(defun find-player-or-die ()
  "Ensure that a recognized player is connected.
Establish necessary dynamics to represent them."
  '(t "tester"))

(defvar *403.json-bytes*
  (flexi-streams:string-to-octets "{\"error\":\"player-not-found\",
\"note\":\"You are not signed in to the web services\",
\"login\":\"https://play.tootsville.org/login/\"}"))

(defvar *json* '(:content-type "application/json"))

(defmacro with-player (() &body body)
  "Ensure that a recognized player is connected
using `FIND-PLAYER-OR-DIE' and bind *PLAYER*"
  `(multiple-value-bind  (foundp *player*)
       (find-player-or-die)
     (cond (foundp
            ,@body)
           (t (return-from endpoint
                (list 403 *json* *403.json-bytes*))))))

(defun assert-my-character (toot-name)
  "Signal a security error if TOOT-NAME is not owned by the active player."
  (check-type toot-name toot-name)
  (error 'unimplemented))

(defendpoint (:get "/users/me" () "application/json") 
  (with-player () 
    (list 200 *json*
          (render-json
           (list :hello "Hello, new user"
                 :fake "This is a totes fake response")))))

(defendpoint (:get "/users/me/toots" () "application/json") 
  (with-player ()
    (list 200 *json*
          (render-json (list :toots
                             (list
                              (list :name "Zap"
                                    :note "These are still fake Toots for testing"
                                    :avatar "UltraToot"
                                    :base-color "violet"
                                    :pattern "lightning"
                                    :pattern-color "yellow"
                                    :highlight-color "yellow"
                                    :child-p nil
                                    :sensitive-p nil
                                    :last-seen (local-time:format-timestring nil (local-time:timestamp- (local-time:now) 3 :day)))
                              (list :name "Flora"
                                    :note "This an an example of a child's Toot appearing on a parent's account"
                                    :avatar "UltraToot"
                                    :base-color "pink"
                                    :pattern "flowers"
                                    :pattern-color "white"
                                    :highlight-color "yellow"
                                    :child-p t
                                    :sensitive-p nil
                                    :last-seen (local-time:format-timestring nil (local-time:timestamp- (local-time:now) 2 :day)))
                              (list :name "Moo"
                                    :note ""
                                    :avatar "UltraToot"
                                    :base-color "white"
                                    :pattern "moo"
                                    :pattern-color "black"
                                    :highlight-color "black"
                                    :child-p nil
                                    :sensitive-p nil
                                    :last-seen (local-time:format-timestring nil (local-time:timestamp- (local-time:now) 1 :day))))))
          )))

(defendpoint (:put "/users/me/toots/:toot-name" (toot-name) "application/json") 
  (with-player ()
    (assert-my-character toot-name) 
    (error 'unimplemented)))

(defendpoint (:post "/users/me/toots" () "application/json") 
  (with-player () 
    (error 'unimplemented)))

(defendpoint (:delete "/users/me/toots/:toot-name" (toot-name)
                      "application/json")
  (with-player ()
    (assert-my-character toot-name) 
    (error 'unimplemented)))

(defendpoint (:get "/toots/:toot-name" (toot-name) "application/json")
  (check-arg-type toot-name toot-name) 
  (with-player () 
    (list 200 *json* (render-json 
                      `(:is-a "toot"
                              :name ,(string-capitalize toot-name)
                              :avatar "ultraToot"
                              :child-p nil
                              :sensitive-p t
                              :online-p t
                              :last-seen ,(local-time:format-timestring nil (local-time:now))
                              :exists-p "maybe?")))))

