(in-package :cl-user)
(defpackage tootsville.view
  (:use :cl)
  (:import-from :tootsville.config
                :*template-directory*)
  (:import-from :caveman2
                :*response*
                :response-headers) 
  (:import-from :datafly
                :encode-json)
  (:export :encode-json
           :render-json))
(in-package :tootsville.view)

(defparameter *template-registry* (make-hash-table :test 'equal))

(defun render-json (object)
  (setf (getf (response-headers *response*) :content-type) "application/json")
  (flexi-streams:string-to-octets (encode-json object)
                                  :external-format :utf-8))


