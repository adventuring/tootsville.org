(in-package :cl-user)
(defpackage tootsville.config
  (:use :cl)
  (:import-from :envy
   :config-env-var
                :defconfig)
  (:export :config
   :*application-root*
           :*static-directory*
   :*template-directory*
           :appenv
   :developmentp
           :productionp))
(in-package :tootsville.config)

(setf (config-env-var) "VIOLETVOLTS")

(defparameter *application-root*
  (asdf:system-source-directory :tootsville))
(defparameter *static-directory* 
  (asdf:system-relative-pathname :tootsville #P"static/"))
(defparameter *template-directory*
  (asdf:system-relative-pathname :tootsville #P"templates/"))

(defconfig :common
    `(:databases ((:maindb :sqlite3 :database-name ":memory:"))
      :on-error-mail (:from-name "Tootsville Support"
                      :from-address "support@tootsville.adventuring.click"
                      :to-address "support@tootsville.adventuring.click"
                      :smtp-server "localhost"
                      :subject-prefix "Error")))

(defconfig |development|
    '())

(defconfig |production|
    '(:error))

(defconfig |test|
    '())

(defun config (&optional key)
  (envy:config #.(package-name *package*) key))

(defun appenv ()
  (uiop:getenv (config-env-var #.(package-name *package*))))

(defvar *developmentp* nil)

(defun developmentp ()
  (if *developmentp*
      (ecase *developmentp*
        (:devel t)
        (:prod nil))
      (let ((developmentp 
             (let ((hostname (machine-instance)))
               (or (search hostname "tootsville.ga")
                   (search hostname "dev.")
                   (search hostname "-dev")
                   (search hostname ".ga'")
                   (not (search hostname "tootsville"))))))
        (setf *developmentp* (if developmentp
                                 :devel
                                 :prod))
        developmentp)))

(defun productionp ()
  (not (developmentp)))
