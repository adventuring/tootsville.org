(in-package :tootsville.web)
(syntax:use-syntax :annot)

(defroute get-/world
    ("/world"
     :method :get :accept '("application/json")) ()
     (setf (getf (response-headers *response*) :content-type) "application/json")
     '(:error))
