all: play login worker servers

####################

clean:
	$(MAKE) -C servers clean
	find . -name \*~ -exec rm {} \;
	rm play/play.js
	rm login/login.js

servers:	servers/Tootsville

servers/Tootsville:
	$(MAKE) -C servers all

####################

WORKERJS = $(shell ./bin/find-worker-js )

worker:	play/lib/worker.js login/lib/worker.js

play/lib/worker.js:	worker/worker.js

login/lib/worker.js:	worker/worker.js

worker/worker.js:	${WORKERJS}
	closure-compiler --create_source_map worker/worker.map \
		--source_map_location_mapping 'worker/|/worker/' \
		${WORKERJS} > worker/worker.js
	echo '//# sourceMappingURL=/worker/worker.map' >> worker/worker.js

####################

play:	play/play.css \
	play/play.js

PLAYJS = $(shell ./bin/find-play-js )

play/play.css:	$(shell find play -name \*.less)
	lessc --strict-math=on --include-path=include --source-map play/play.less play/play.css

play/play.js:	${PLAYJS}
	closure-compiler --create_source_map play/play.map \
		--source_map_location_mapping 'play/|/play/' \
		${PLAYJS} > play/play.js
	echo '//# sourceMappingURL=/play/play.map' >> play/play.js

####################

login:	login/login.css \
	login/login.js

login/login.css:	$(shell find login -name \*.less)
	lessc --strict-math=on --include-path=include --source-map login/login.less login/login.css

LOGINJS=$(shell ./bin/find-login-js)

login/login.js:	${LOGINJS}
	closure-compiler --create_source_map login/login.map \
		--source_map_location_mapping 'login/|/login/' \
		${LOGINJS} \
		> login/login.js
	echo '//# sourceMappingURL=/login/login.map' >> login/login.js
