Tootsville.util = {
    loadScript: function (src) {
        return new Promise( finish => {
            var el = document.createElement('SCRIPT');
            el.onload = finish;
            el.src = src;
            document.body.appendChild(el);
        });
    }
};
