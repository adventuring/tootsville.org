if (! Tootsville.util) { Tootsville.util = {}; }

Tootsville.util.postJSONforJSON = function (uri, post, headers) {
    if (!post) { post = {}; }
    if (!headers) { headers = {}; }
    return new Promise( (pass, fail) => {
        xhr.open('POST', uri);
        xhr.onload = (response) => {
            pass(JSON.parse(response.body));
        };
        xhr.onerror = (failure) => { fail(failure); };
        for (header in headers) {
            if (headers.hasOwnProperty(header)) {
                xhr.setHeader(header, headers[header]);
            }
        }
        xhr.setHeader('Accept', 'application/json;encoding=utf-8');
        xhr.setHeader('Content-Type', 'application/json;encoding=utf-8');
        xhr.send(JSON.strigify(post));
    });
};

Tootsville.util.assertValidHost = function (hostName) {
    return (['users', 'gossip', 'indira'].indexOf(hostName) > -1)
}

Tootsville.util.rest = function (uri, post, headers) {
    var hostName = uri.split('/')[1];
    Tootsville.util.assertValidHostName(hostName);
    return Tootsville.util.postJSONforJSON('https://' + hostName + '.' + Tootsville.cluster + uri,
                                           post, headers);
};
